clear

% Get path of folder with excel files
folder_name = uigetdir;
if folder_name==0
    break
else
    folder_name=strcat(folder_name,'\');
end
pathname = folder_name;

% Read all filenames in the folder
filesInFolder = dir(strcat(pathname,'*'));filesInFolder(1:2)=[];
N_f = length(filesInFolder);
filenames = cell(1,N_f);
for f = 1:N_f
    filenames(f) = {filesInFolder(f).name};
end

% Perform checks on input and sanitize if necessary
filenames = performChecksOnFilenamesInput(filenames);

try
    counter = 0;
    for filename = filenames
        counter = counter+1;
        % Initialize waitbar
        h = waitbar(0,['Loading excel file ' filename{1} ', which is file ' num2str(counter) ' out of ' num2str(size(filenames,2)) '. This may take some time.'],...
            'CreateCancelBtn','setappdata(gcbf,''canceling'',1)');
            setappdata(h,'canceling',0)
            set(findall(h), 'Units', 'Normalized')
            set(h, 'Units', 'Pixels', 'Position', [500 500 750 100])
        
        % Initialize filenames
        filenameIn = strcat(pathname,filename{1});
        filenameOut = strcat(pathname,'Formatted',filename{1});
        
        % Determine which sheet contains the information
        sheetNumber = determineWhichSheetToUse(filenameIn);
        
        % Check whether cancel button is pushed
        if getappdata(h,'canceling')
            break
        end
        
        % Import data
        if ~isempty(sheetNumber)
            data = importfile(filenameIn, sheetNumber);
            
            % Check whether cancel button is pushed
            if getappdata(h,'canceling')
                break
            end
            
            % Find number of variables per security
            NonEmptyCellsFirstRow = ~cell2mat(cellfun(@isempty,data(1,:),'UniformOutput',false));
            FirstNonEmptyCellFirstRow = find(NonEmptyCellsFirstRow==1,1);
            SecondNonEmptyCellFirstRow = find(NonEmptyCellsFirstRow==1,2);SecondNonEmptyCellFirstRow = SecondNonEmptyCellFirstRow(2);
            N_vars = SecondNonEmptyCellFirstRow - FirstNonEmptyCellFirstRow - 1;
            
            % Store variable names
            varNames = data(2,1:N_vars);
            varNames = regexprep(varNames,'[&]','');
            
            % Find number of securities
            N_sec = sum(NonEmptyCellsFirstRow);
            
            % Set up the structure of the table that will hold outputdata, by filling
            % in the data of the first security
            formattedData = cell2table(cell(0,size(varNames,2)+1),'VariableNames',[{'date','Ticker'} varNames(2:end)]);
            LocNextTicker = 1;
            
            for i = 1:N_sec
                % Check whether execution is cancelled
                if getappdata(h,'canceling')
                    break
                end
                
                % Put the data belonging to the ticker specified in LocNextTicker
                % in the right format
                DateFormatIn = 'dd/mm/yyyy';
                formattedData = reformatDataOfSpecificSecurity(formattedData,data,varNames,LocNextTicker,N_vars,DateFormatIn);
                
                % Find the lcoation of the next ticker
                LocNextTicker = LocNextTicker + N_vars + 1;
                
                % Update waitbar
                waitbar(i/N_sec,h,['Reformatting excel file ' filename{1} ', sheet ' num2str(sheetNumber) '; security ' num2str(i) ' of ' num2str(N_sec)]);
            end
            formattedData = formattedData(:,[2 1 3:end]);
            
            % Check whether execution is cancelled
            if getappdata(h,'canceling')
                break
            end
            
            % Write table away
            waitbar(1,h,['Writing formatted data of ' filename{1} ', sheet ' num2str(sheetNumber) ' to excel; this is file ' num2str(counter) ' out of ' num2str(size(filenames,2)) '. This may take some time.']);
            writetable(formattedData,filenameOut)
            if getappdata(h,'canceling')
                break
            end
            delete(h)
        else
            error('Please make sure your excel files have the right structure. The raw bloomberg excel output should start in the top left corner of the second sheet (i.e. first ticker should be in cell A1 of the second sheet).')
        end
    end
catch
    delete(h)
    error('Please make sure your excel files have the right structure. The raw bloomberg excel output should start in the top left corner of the second sheet (i.e. first ticker should be in cell A1 of the second sheet).')
end
delete(h)