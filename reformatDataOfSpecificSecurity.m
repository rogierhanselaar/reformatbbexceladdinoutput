function formattedData = reformatDataOfSpecificSecurity(formattedData,data,varNames,LocNextTicker,N_vars,DateFormat)
temp=table();

% Store date format
formatOut = 'yyyymmdd';

% Check whether date format is string or number
EmptyCellsDate = cell2mat(cellfun(@isempty,data(:,LocNextTicker),'UniformOutput',false));
DateIsString=cellfun(@isstr,data(3,LocNextTicker));
if DateIsString==1
    EmptyCellsDate(1:3)=1;
else
    EmptyCellsDate(1:2)=1;
end

% Put the data in the right place in the table temp
if sum(~EmptyCellsDate)~=0
    if DateIsString==1
        temp.date = ['00000000';datestr(datenum(data(~EmptyCellsDate,LocNextTicker),DateFormat),formatOut)]; % the first date sometimes gives an error in the output, hence the empty cell in the first column
        EmptyCellsDate(3)=0;
    else
        temp.date = datestr(x2mdate(cell2mat(data(~EmptyCellsDate,LocNextTicker))),formatOut);
    end
    temp.Ticker = repmat(data(1,LocNextTicker),size(temp.date,1),1);
    for j = 2:N_vars
        temp.(varNames{j}) = data(~EmptyCellsDate,LocNextTicker+j-1);
    end
    formattedData = [formattedData;temp];
end

end