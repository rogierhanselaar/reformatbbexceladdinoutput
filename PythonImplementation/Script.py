import os
import pandas as pd
import tkinter as tk

from tkinter import filedialog
import Functions_for_script as ffs
import Classes_for_script as cfs

# load excel file here and determine which sheets need conversion
root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename()
#pathname = "C:/Users/rogie/OneDrive/Documents/Work/ERIM PhD in Finance/Onderwijs/Datateam/Nuttige files/ReorganiseBBExcelOutputInMatlab/20160302ClosedEndFundData/"
#filename = "ClosedEndFundData.xlsx"
pathname = os.path.dirname(file_path) + '/'
filename = os.path.basename(file_path)
data = pd.ExcelFile(pathname + filename)
sheets_needing_conversion = ffs.determine_which_sheet_needs_conversion(data)

for i in sheets_needing_conversion:
    # Create data_sheet instance
    data_sheet = cfs.data_sheet(data,i)
    for j in data_sheet.location_of_identifiers:
        data_sheet.add_security(j)
    
    # Create a DataFrame containing formatted data
    formatted_data_sheet = pd.DataFrame()
    for j in range(0,data_sheet.number_of_securities):
        formatted_data_sheet = formatted_data_sheet.append(data_sheet.format_security(j), ignore_index = True)
        
    # Write the DataFrame to excel
    formatted_data_sheet.to_excel(pathname + 'Formatted' + filename,sheet_name='Sheet'+str(i))


    
    