# -*- coding: utf-8 -*-
"""
Created on Mon May  1 11:27:18 2017

@author: rogie
"""

import numpy as np

class data_sheet(object):
    """
    A sheet with data on securities that needs to be reformatted.
    """
    
    def __init__(self, data, sheet_number):
        self.sheet_number = sheet_number
        self.data = data.parse(sheet_number)
        self.location_of_identifiers = self._find_location_of_identifiers()
        self._test_whether_identifiers_are_evenly_spaced()
        self.number_of_securities = self.location_of_identifiers.size
        self.number_of_variables = self.location_of_identifiers[1]-self.location_of_identifiers[0] - 1
        self.names_of_variables = self.data.iloc[0,:self.number_of_variables].tolist()
        self.securities = []
        
    def _find_location_of_identifiers(self):
        column_names = list(self.data.columns.values)
        self.location_of_identifiers  = np.array([j for j,x in enumerate(column_names) if "Unnamed" not in x])
        return self.location_of_identifiers
    
    def _test_whether_identifiers_are_evenly_spaced(self):
        if np.size(np.unique(self.location_of_identifiers[1:-1]-self.location_of_identifiers[0:-2]))==1:
            return
        else:
            raise ValueError('On sheet ' + str(self.sheet_number) + ', the identifiers do not seem to be evenly spaced. If you want to have this sheet converted, please only upload the raw Bloomberg output; if not, remove this sheet from your excel file.')
            
    def add_security(self,j):
        self.securities.append(Security(self,j))
        
    def format_security(self,j):
        formatted_data = self.data.iloc[self.securities[j].rows,self.securities[j].columns]
        formatted_data.columns = self.names_of_variables
        formatted_data['Identifiers'] = self.securities[j].identifier
        cols = formatted_data.columns.tolist()
        cols = cols[-1:] + cols[:-1]
        formatted_data = formatted_data[cols]
        return formatted_data
        


class Security(object):
    """
    A security, defined by information on the location of scurity level data
    """
    
    def __init__(self,data_sheet,j):
        self.columns = range(j,j+data_sheet.number_of_variables)
        self.rows = range(1,data_sheet.data.iloc[1:,self.columns].dropna(thresh=data_sheet.number_of_variables-1).shape[0]+1)
        self.number_of_observations = data_sheet.data.iloc[1:,j+1].count() # j+1 as occasionally the first date is NaN
        self.identifier = data_sheet.data.columns[j]