# -*- coding: utf-8 -*-
"""
Created on Sun Apr 23 12:36:31 2017

Functions for script

@author: rogie
"""

import numpy as np

def determine_which_sheet_needs_conversion(data):
    sheet_names = data.sheet_names
    n_sheets = len(sheet_names)
    sheets_needing_conversion = np.array([],dtype=np.int)
    
    for i in range(0,n_sheets-1):
        location_identifiers = find_location_of_identifiers(data,i)
        if location_identifiers.size > 1:
            if location_identifiers[1]-location_identifiers[0]>=3:
                sheets_needing_conversion = np.hstack((sheets_needing_conversion,np.array([i])))

    return sheets_needing_conversion

def find_location_of_identifiers(data,i):
    data_sheet = data.parse(i)
    column_names = list(data_sheet.columns.values)
    location_identifiers  = np.array([j for j,x in enumerate(column_names) if "Unnamed" not in x])
    return location_identifiers
