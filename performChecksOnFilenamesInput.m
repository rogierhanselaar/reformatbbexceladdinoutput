function filenames = performChecksOnFilenamesInput(filenames)

% Check whether each file has .xlsx or .xls as extension; if not mark it
XlsxExtension = cellfun(@(x) x(end-4:end),filenames,'UniformOutput',false);
IndexRemove = zeros(size(filenames));
for i = 1:size(XlsxExtension,2)
    if ~strcmpi(XlsxExtension(i),'.xlsx')
        if ~strcmpi(XlsxExtension{i}(1:end-1),'.xls')
            IndexRemove(i)=1;
        end
    end
end

% Mark files that are already formatted from the list, or that are
% temporary files of files that are currently open in excel 
for i = 1:size(filenames,2)
    if strcmpi(filenames{i}(1:9),'Formatted')==1
        IndexRemove(i)=1;
    end
    if strcmpi(filenames{i}(1:2),'~$')==1
        IndexRemove(i)=1;
    end
end

% Remove files from the list
filenames(IndexRemove==1)=[];


end