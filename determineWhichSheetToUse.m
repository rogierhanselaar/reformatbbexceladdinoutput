function sheetNumber = determineWhichSheetToUse(filename)

[~,sheets] = xlsfinfo(filename);
numOfSheets = numel(sheets);

SheetPotentiallyWithRelevantData = [];
for i = 1:numOfSheets
    sampleFromSheet = importfile(filename, i, 'A1:XFD1');
    NonEmptyCellsFirstRow = ~cell2mat(cellfun(@isempty,sampleFromSheet(1,:),'UniformOutput',false));
    FirstNonEmptyCellFirstRow = find(NonEmptyCellsFirstRow==1,1);
    SecondNonEmptyCellFirstRow = find(NonEmptyCellsFirstRow==1,2);SecondNonEmptyCellFirstRow = SecondNonEmptyCellFirstRow(2);
    if ~isempty(FirstNonEmptyCellFirstRow) && ~isempty(SecondNonEmptyCellFirstRow)
        if SecondNonEmptyCellFirstRow - FirstNonEmptyCellFirstRow >= 3
            SheetPotentiallyWithRelevantData = [SheetPotentiallyWithRelevantData,i];
        end
    end
end

if isempty(SheetPotentiallyWithRelevantData)
    sheetNumber=[];
elseif size(SheetPotentiallyWithRelevantData,2)>1 && sum(SheetPotentiallyWithRelevantData==2)==1
    sheetNumber=2;
elseif size(SheetPotentiallyWithRelevantData,2)>1
    sheetNumber = min(SheetPotentiallyWithRelevantData);
else
    sheetNumber = SheetPotentiallyWithRelevantData;
end
    


end